# Install Python requirements
Project uses Python 3.8. Requirements are listed in `requirements.txt` file. 
In order to install them is needed to activate Python environment and run a command: 

Create Python virtual environment 

    python -m venv scratch-detection

Activate Python virtual environment

    .\scratch-detection\Scripts\activate.bat

Go to project root directory and then install requirements

    pip install --upgrade pip && pip install -r requirements.txt

# Image segmentation model usage
Image segmentation is implemented in `SegmentationClient` class. To create class instance is required to pass parameters:

`model_path` - path to segmentation model saved in `.h5` format;
`model_backbone` - segmentation model backbone;
`model_optimizer` - segmentation model optimizer;
`model_loss` - segmentation model loss function.

## Usage example
### Create Segmentation client instance
    segmentation_client = SegmentationClient(
        model_path="C:/Path/To/Folder/With/Model/",
        model_backbone="resnet152",
        model_optimizer=SGD(),
        model_loss=binary_crossentropy
    )

### Recognize defect on images in folder
    segmentation_client.predict_images("C:/Path/To/Folder/With/Images/")
Result images will be saved into "/predicted" subfolder.

### Find region properties on segmented images
    binary_threshold = 128
    segmentation_client.calc_region_props_in_folder(
        folder_path="C:/Path/To/Folder/With/Images/predicted/",
        binary_threshold=binary_threshold,
        result_csv_path=f"C:/Path/To/CSV/File/regions-thr{binary_threshold}.csv"
    )
Parameter `folder_path` contain path to recognized defects images.
`binary_threshold` is a segmentation threshold of defects in recognized images. 