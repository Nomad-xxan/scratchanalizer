import os.path

import matplotlib.pyplot as plt
import pandas as pd

from constants import *


class RegPropAnalyzer:

    def __init__(self, regprop_parameters, min_area=0):
        self.regprop_df = pd.read_csv(regprop_parameters, delimiter=",")

        if min_area > 0:
            self.regprop_df = self.regprop_df[self.regprop_df[field_filled_area] > min_area]

        self.regprop_df[field_zone] = self.regprop_df[field_image_name].str.slice(8, 9).astype(int)

        self.regprop_df[field_illumination_str] = self.regprop_df[field_image_name].str.slice(2, 5)
        # self.regprop_df[field_illumination] = self.regprop_df.apply(lambda row: self._illumination_range(row), axis=1)
        self.regprop_df[field_illumination] = self.regprop_df.apply(lambda row: int(row[field_illumination_str]), axis=1)

        self.illumination_list = sorted(set(self.regprop_df[field_illumination].to_list()))

    def _illumination_range(self, row):
        illum = int(row[field_illumination_str])
        if illum < 10:
            illumination = 2
        elif illum < 100:
            illumination = round(illum, -1)
        else:
            illumination = round(illum, -2)

        return illumination

    def data_by_zone(self, zones):
        return self.regprop_df[self.regprop_df[field_zone].isin(zones)]

    def build_param_by_illuminance_box_plot(self, df, param, show_plot=True, save_plot_path=""):
        illuminations = sorted(set(df[field_illumination].to_list()))
        params = [df[df[field_illumination] == ill][param].to_list() for ill in illuminations]

        plt.rc('axes', edgecolor=color_frame)
        fig1, ax1 = plt.subplots()
        ax1.set_title(f"{param} by Illumination Statistics")
        ax1.set_xlabel(f"{field_illumination}, Lx")
        ax1.set_ylabel(param)
        boxpl = ax1.boxplot(
            params,
            vert=True,
            patch_artist=True,
            labels=illuminations
        )

        for item in ['boxes', 'whiskers', 'fliers', 'caps']:
            plt.setp(boxpl[str(item)], color=color_bar)
        plt.setp(boxpl["medians"], color=color_median)
        plt.setp(boxpl["boxes"], facecolor=color_bar)
        plt.setp(boxpl["fliers"], markeredgecolor=color_bar)

        if show_plot:
            plt.show()
        if save_plot_path:
            fig1.savefig(os.path.join(save_plot_path, f"{param}-by-Illum-Box.png"))

    def build_param_by_illuminance_scatter_plot(self, df, param, show_plot=True, save_plot_path=""):
        plt.rc('axes', edgecolor=color_frame)
        fig1, ax1 = plt.subplots()
        ax1.set_title(f"{param} Values")
        ax1.set_xlabel(f"{field_illumination}, Lx")
        ax1.set_ylabel(param)
        sctpl = ax1.scatter(
            x=df[field_illumination],
            y=df[param],
            c=color_bar,
        )

        if show_plot:
            plt.show()
        if save_plot_path:
            fig1.savefig(os.path.join(save_plot_path, f"{param}-by-Illum-Scatter.png"), bbox_inches='tight')

    def graph_area_by_threshold(self, illumination, show_plot=True, save_plot_path=""):
        areas = [df[df[field_illumination] == illumination][field_filled_area].sum() / 1000 for df in self._df12_list]

        fig, ax = plt.subplots()
        ax.plot(self._thresholds, areas)

        ax.set(
            xlabel="threshold",
            ylabel="Filled Area, x1000 pix",
            title=f"Filled Area by Threshold (Illumination {illumination})"
        )
        ax.grid()

        if show_plot:
            plt.show()
        if save_plot_path:
            fig.savefig(os.path.join(
                save_plot_path,
                f"Filled-Area-by-Threshold-illum-{illumination:03}.png")
            )