import os.path

from constants import *
from regions_analysis.threshold_prop_analyzer import ThresholdPropAnalyzer
from regions_analysis.reg_prop_analyzer import RegPropAnalyzer


class DefectAnalyzer:

    def __init__(self,
                 min_area,
                 zones,
                 threshold,
                 thresholds,
                 illumination_short_list):
        self.min_area = min_area
        self.zones = zones
        self.threshold = threshold
        self.thresholds = thresholds
        self.illumination_short_list = illumination_short_list

    def calc_param_statistics(self):
        reg_prop_analyzer = RegPropAnalyzer(
            regprop_parameters=os.path.join(regprop_path, f"regions-thr{self.threshold}.csv"),
            min_area=self.min_area
        )
        df12 = reg_prop_analyzer.data_by_zone(self.zones)

        for param in ANALYZED_PARAMS:
            reg_prop_analyzer.build_param_by_illuminance_box_plot(
                df=df12,
                param=param,
                save_plot_path=os.path.join(ROOT_DIR, "plots")
            )
            reg_prop_analyzer.build_param_by_illuminance_scatter_plot(
                df=df12,
                param=param,
                save_plot_path=os.path.join(ROOT_DIR, "plots")
            )

    def analyze_thresholds(self):
        threshold_analyzer = ThresholdPropAnalyzer(
            files_path=regprop_path,
            files_list=[f"regions-thr{threshold:03}.csv" for threshold in self.thresholds],
            min_area=600
        )
        for illumination in self.illumination_short_list:
            threshold_analyzer.graph_area_by_threshold(
                illumination=illumination,
                show_plot=True,
                save_plot_path=os.path.join(ROOT_DIR, "plots")
            )

            threshold_analyzer.graph_orientation_by_threshold(
                illumination=illumination,
                show_plot=True,
                save_plot_path=os.path.join(ROOT_DIR, "plots")
            )

        threshold_analyzer.build_all_param_by_threshold_box_plot(
            save_plot_path=os.path.join(ROOT_DIR, "plots")
        )


if __name__ == '__main__':

    print("Images analysis started")

    defect_analyzer = DefectAnalyzer(
        min_area=50, #600,
        zones=[1],
        threshold=200,
        thresholds=[51, 102, 128, 175, 200],
        illumination_short_list=[20, 36, 70, 108, 149, 184, 233, 270]  #[37, 64, 98, 131, 159, 196, 220, 248, 275, 312, 324]
    )
    defect_analyzer.calc_param_statistics()
    defect_analyzer.analyze_thresholds()

    print("Images analysis finished")
