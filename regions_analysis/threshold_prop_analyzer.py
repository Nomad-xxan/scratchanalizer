import math
import os.path

import matplotlib.pyplot as plt

from constants import *
from regions_analysis.reg_prop_analyzer import RegPropAnalyzer


class ThresholdPropAnalyzer:

    def __init__(self, files_path, files_list, min_area=0):

        self._reg_analyzers = [
            RegPropAnalyzer(
                regprop_parameters=os.path.join(files_path, file_name),
                min_area=min_area
            )
            for file_name in files_list
        ]
        self._thresholds = sorted([int(file_name[11:14]) for file_name in files_list])

        self._df12_list = [analyzer.data_by_zone([1, 2]) for analyzer in self._reg_analyzers]

    def graph_area_by_threshold(self, illumination, show_plot=True, save_plot_path=""):
        areas = [df[df[field_illumination] == illumination][field_filled_area].sum() / 1000 for df in self._df12_list]

        fig, ax = plt.subplots()
        ax.plot(self._thresholds, areas)

        ax.set(
            xlabel="threshold",
            ylabel="Filled Area, x1000 pix",
            title=f"Filled Area by Threshold (Illumination {illumination})"
        )
        ax.grid()

        if show_plot:
            plt.show()
        if save_plot_path:
            fig.savefig(os.path.join(
                save_plot_path,
                f"Filled-Area-by-Threshold-illum-{illumination:03}.png")
            )

    def graph_orientation_by_threshold(self, illumination, show_plot=True, save_plot_path=""):
        orientations = [df[df[field_illumination] == illumination][field_orientation].mean() for df in self._df12_list]

        fig, ax = plt.subplots()
        ax.plot(self._thresholds, orientations)

        ax.set(
            xlabel="threshold",
            ylabel="Orientation, rad",
            title=f"Mean Orientation by Threshold (Illumination {illumination})"
        )
        ax.grid()
        ax.set_ylim(-math.pi / 2, math.pi / 2)

        if show_plot:
            plt.show()
        if save_plot_path:
            fig.savefig(os.path.join(
                save_plot_path,
                f"Orientation-by-Threshold-illum-{illumination:03}.png")
            )

    def build_param_by_threshold_box_plot(self, illumination, param, show_plot=True, save_plot_path=""):
        params = [df[df[field_illumination] == illumination][param].to_list() for df in self._df12_list]

        plt.rc('axes', edgecolor=color_frame)
        fig1, ax1 = plt.subplots()
        ax1.set_title(f"{param} by Threshold Statistics (Illumination {illumination})")
        ax1.set_xlabel("Threshold")
        ax1.set_ylabel(param)
        boxpl = ax1.boxplot(
            params,
            vert=True,
            patch_artist=True,
            labels=self._thresholds
        )

        for item in ['boxes', 'whiskers', 'fliers', 'caps']:
            plt.setp(boxpl[str(item)], color=color_bar)
        plt.setp(boxpl["medians"], color=color_median)
        plt.setp(boxpl["boxes"], facecolor=color_bar)
        plt.setp(boxpl["fliers"], markeredgecolor=color_bar)

        if show_plot:
            plt.show()
        if save_plot_path:
            fig1.savefig(os.path.join(save_plot_path, f"{param}-by-Thresh-Illum-{illumination:03}-Box.png"))

    def build_all_param_by_threshold_box_plot(self, save_plot_path):
        illuminations = sorted(set(self._df12_list[0][field_illumination].to_list()))

        for param in ANALYZED_PARAMS:
            for illumination in illuminations:
                self.build_param_by_threshold_box_plot(
                    illumination=illumination,
                    param=param,
                    show_plot=True,
                    save_plot_path=save_plot_path
                )
