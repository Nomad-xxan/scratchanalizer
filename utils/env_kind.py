from enum import IntEnum, auto


class EnvironmentKind(IntEnum):
    """
    Kinds of execution environments
    """
    dev = auto()
    prod = auto()
