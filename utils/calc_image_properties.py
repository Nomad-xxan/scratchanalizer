import os
import cv2
from utils.img_region_properties import calc_image_regions_properties

if __name__ == "__main__":
    folder_path = "D:\\Progs\\.NET\\ImageProcessor\\ImageProcessor\\bin\\Debug\\11"
    image_files = ["1_x500_07-segm.jpg", "5_x500_02-segm.jpg", "7_x500_02-segm.jpg", "9_x500_02-segm.jpg", "11_x500_02-segm.jpg"]

    for file_name in image_files:
        print(f"Processing image: {file_name}")
        img = cv2.imread(os.path.join(folder_path, file_name), cv2.IMREAD_GRAYSCALE)
        df = calc_image_regions_properties(img=img, img_name=file_name)
        df.to_csv(os.path.join(folder_path, file_name + ".csv"), header=True, sep=",")
