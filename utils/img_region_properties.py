import pandas as pd
from skimage.measure import label, regionprops


def calc_image_regions_properties(img, img_name):
    label_img = label(img)
    regions = regionprops(label_img)

    dfr = pd.DataFrame(
        columns=["Image_Name", "Area", "Filled_Area", "Centroid", "Eccentricity", "Equivalent_Diameter", "Feret_Diameter_Max",
                 "Major_Axis_Length", "Minor_Axis_Length", "Orientation", "Perimeter"])

    for props in regions:
        dfr = dfr.append({
            "Image_Name": img_name,
            "Area": props.area,
            "Filled_Area": props.filled_area,
            "Centroid": props.centroid,
            "Eccentricity": props.eccentricity,
            "Equivalent_Diameter": props.equivalent_diameter,
            "Feret_Diameter_Max": props.feret_diameter_max,
            "Major_Axis_Length": props.major_axis_length,
            "Minor_Axis_Length": props.minor_axis_length,
            "Orientation": props.orientation,
            "Perimeter": props.perimeter
        }, ignore_index=True)

    return dfr
