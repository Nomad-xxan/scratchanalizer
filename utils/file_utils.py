from os import listdir
from os.path import isfile, join


def create_files_list(images_path, include_path=True):
    files = [f for f in listdir(images_path) if isfile(join(images_path, f))]
    if include_path:
        files = list(map(lambda x: images_path + x, files))
    files_count = len(files)
    print("Total count of files in folder {0}: {1}".format(images_path, files_count))

    return files

