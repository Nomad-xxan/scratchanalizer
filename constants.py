import os

from utils.env_kind import EnvironmentKind

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

os.environ["CUDA_VISIBLE_DEVICES"] = "0"


# CNN segmentation model file name
model_name = "seg-unet-v02a-E057_loss-0.0157_val_loss-0.0201.h5"

# Path
ENV = EnvironmentKind.dev
if ENV == EnvironmentKind.dev:
    model_storage = "d:/Progs/Python/SurfaceDefects/models/models_storage/"
    images_path = "D:/Documents/Docs/Science/2023/03/Images/2/renamed/"
    regprop_path = "D:/Documents/Docs/Science/2023/03/Images/2/renamed/regions/"
    # images_path = "d:/Documents/Docs/Science/2021/01/samples/crops/"
    # regprop_path = "d:/Documents/Docs/Science/2021/01/samples/"
elif ENV == EnvironmentKind.prod:
    model_storage = "c:/PythonProjects/surfacedefects/models/models_storage/"
    images_path = "e:/Students/2021-Stud-Sci-Work/crops/"
    regprop_path = "e:/Students/2021-Stud-Sci-Work/"

PREDICTED_FOLDER = "predicted"
BINARY_THRESHOLD = 200
MODEL_INPUT_SHAPE = (256, 256)

# Field names
field_image_name = "Image_Name"
field_area = "Area"
field_filled_area = "Filled_Area"
field_zone = "Zone"
field_illumination = "Illumination"
field_illumination_str = "IlluminationStr"
field_orientation = "Orientation"

# Colors
color_frame = "royalblue"
color_bar = "cornflowerblue"
color_median = "azure"

# ANALYZED_PARAMS = ["Filled_Area", "Eccentricity", "Major_Axis_Length", "Minor_Axis_Length", "Orientation", "Perimeter"]
ANALYZED_PARAMS = ["Filled_Area", "Orientation"]