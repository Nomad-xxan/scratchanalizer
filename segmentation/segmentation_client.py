import os
from enum import IntEnum, auto

import cv2
import numpy as np
import pandas as pd
from segmentation_models import get_preprocessing
from tensorflow.keras.models import load_model

from constants import MODEL_INPUT_SHAPE, PREDICTED_FOLDER
from utils.file_utils import create_files_list
from utils.img_region_properties import calc_image_regions_properties


class PatchOverlapMethod(IntEnum):
    """
    Methods of calculation of pixels for overlapped parts of patches
    """
    max = auto()
    mean = auto()


class SegmentationClient:

    def __init__(self,
                 model_path,
                 model_backbone,
                 model_optimizer,
                 model_loss,
                 model_input_shape=MODEL_INPUT_SHAPE):

        self.model_input = model_input_shape
        self.preprocess = get_preprocessing(model_backbone)

        optimizer = model_optimizer
        loss = model_loss
        self.model = load_model(model_path, compile=False)
        self.model.compile(optimizer=optimizer, loss=loss)

    def predict_standard_image(self, file_path):
        """
        Predict image that has 'standard' size defined by MODEL_INPUT_SHAPE constant.
        :param file_path: str, Path to image
        :return: numpy array of predicted image
        """
        img = self.preprocess(cv2.imread(file_path))
        img = np.expand_dims(img, axis=0)
        res = self.model.predict(img)
        return res[0] * 255

    def predict_standard_images(self, folder_path):
        """
        Predict images that have 'standard' size (defined by MODEL_INPUT_SHAPE constant)
        in the given folder. All predicted images are saved into separate PREDICTED_FOLDER
        subfolder.
        :param folder_path: str, Path to folder that contains images.
        """
        if not folder_path.endswith("/"):
            folder_path = folder_path + "/"

        images_to_predict = create_files_list(folder_path, False)

        if not os.path.exists(f"{folder_path}{PREDICTED_FOLDER}/"):
            os.makedirs(f"{folder_path}{PREDICTED_FOLDER}/")

        for file_name in images_to_predict:
            cv2.imwrite(
                f"{folder_path}{PREDICTED_FOLDER}/" + file_name,
                self.predict_standard_image(folder_path + file_name)
            )

    def predict_image(self, file_path, overlap_method=PatchOverlapMethod.max):
        """
        Predict image with any (non-standard size) size
        :param file_path: str, Path to image
        :param overlap_method: PatchOverlapMethod, method of calculation of pixels
            for overlapped parts of patches
        :return:
        """
        img = self.preprocess(cv2.imread(file_path))
        (image_height, image_width, _) = img.shape
        predicted = np.zeros((image_height, image_width, 1), np.uint8)

        (input_h, input_w) = MODEL_INPUT_SHAPE
        (shift_h, shift_w) = (input_h // 3 * 2, input_w // 3 * 2)

        x0 = 0
        pass_x = True
        while pass_x:
            x1 = x0 + input_w
            if x1 > image_width:
                x1 = image_width
                x0 = x1 - input_w
            if x1 == image_width:
                pass_x = False

            y0 = 0
            pass_y = True
            while pass_y:
                y1 = y0 + input_h
                if y1 > image_height:
                    y1 = image_height
                    y0 = y1 - input_h
                if y1 == image_height:
                    pass_y = False

                crop = np.expand_dims(img[y0:y1, x0:x1], axis=0)
                res = self.model.predict(crop)
                predicted_patch = res[0] * 255
                if overlap_method == PatchOverlapMethod.max:
                    predicted[y0:y1, x0:x1] = np.maximum(predicted[y0:y1, x0:x1], predicted_patch)
                elif overlap_method == PatchOverlapMethod.mean:
                    predicted[y0:y1, x0:x1] = np.mean([predicted[y0:y1, x0:x1], predicted_patch], axis=0)

                y0 = y0 + shift_h

            x0 = x0 + shift_w

        return predicted

    def predict_images(self, folder_path):
        """
        Predict images that have 'non-standard' size (defined by MODEL_INPUT_SHAPE constant)
        in the given folder. All predicted images are saved into separate PREDICTED_FOLDER
        subfolder.
        :param folder_path: str, Path to folder that contains images.
        """
        if not folder_path.endswith("/"):
            folder_path = folder_path + "/"

        images_to_predict = create_files_list(folder_path, False)

        if not os.path.exists(f"{folder_path}{PREDICTED_FOLDER}/"):
            os.makedirs(f"{folder_path}{PREDICTED_FOLDER}/")

        for file_name in images_to_predict:
            cv2.imwrite(
                f"{folder_path}{PREDICTED_FOLDER}/" + file_name,
                self.predict_image(folder_path + file_name)
            )

    @staticmethod
    def calc_region_props_in_folder(folder_path, binary_threshold, result_csv_path, save_segmented_images=False):
        """
        Calculate regions properties in segmented images and save them
        into CSV file.
        :param binary_threshold: int, Threshold of Gray->Binary image transform
        :param folder_path: str, Path to folder with segmented images
        :param result_csv_path: str, Results CSV file full path
        :param save_segmented_images: bool, Save segmented images or not
        :return: Pandas DataFrame with regions properties
        """
        files = create_files_list(folder_path, include_path=False)

        df = None
        for file in files:
            file_name, file_extension = os.path.splitext(file)
            img_segmented = cv2.imread(os.path.join(folder_path, file))
            img_segmented = cv2.cvtColor(img_segmented, cv2.COLOR_BGR2GRAY)
            img_segmented[img_segmented < binary_threshold] = 0
            img_segmented[img_segmented >= binary_threshold] = 255

            if save_segmented_images:
                cv2.imwrite(os.path.join(folder_path, "thresholded", f"{file_name}-thr{binary_threshold:03}.png"), img_segmented)

            dfi = calc_image_regions_properties(cv2.transpose(img_segmented), file)
            df = dfi if df is None else pd.concat([df, dfi], ignore_index=True).reset_index(drop=True)

        df.to_csv(result_csv_path, index=False)
        return df
