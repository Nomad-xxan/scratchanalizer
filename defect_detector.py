from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.optimizers import SGD

from constants import *
from segmentation.segmentation_client import SegmentationClient


class DefectDetector:

    def __init__(self, model_storage_path, model_file_name, model_backbone, model_optimizer, model_loss):
        self.model_storage = model_storage_path
        self.model_name = model_file_name
        self.model_backbone = model_backbone
        self.model_optimizer = model_optimizer
        self.model_loss = model_loss

    def __call__(self, *args, **kwargs):
        segmentation_client = SegmentationClient(
            model_path=os.path.join(self.model_storage, self.model_name),
            model_backbone=self.model_backbone,
            model_optimizer=self.model_optimizer,
            model_loss=self.model_loss
        )

        segmentation_client.predict_images(os.path.join(images_path))

        segmentation_client.calc_region_props_in_folder(
            folder_path=os.path.join(images_path, "predicted"),
            binary_threshold=BINARY_THRESHOLD,
            result_csv_path=os.path.join(images_path, "predicted", f"-regions-thr{BINARY_THRESHOLD}.csv")
        )


if __name__ == '__main__':

    print("Defects detection started")

    defect_detector = DefectDetector(
        model_storage_path=model_storage,
        model_file_name=model_name,
        model_backbone="resnet152",
        model_optimizer=SGD(),
        model_loss=binary_crossentropy
    )
    defect_detector()

    print("Defects detection finished successfully")
